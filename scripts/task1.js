"use strict";
/* 
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

Завдання
Написати програму "Я тебе знайду по IP"

Технічні вимоги:

Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await. */

console.log("TASK 1");
console.log("THEORY");
console.log("Асинхронність в JavaScript - це концепція, що дозволяє виконувати операції без блокування основного потоку виконання коду. В основі асинхронності лежить використання колбеків (callback functions), промісів (promises) та асинхронних функцій (async/await).");

const requestURL = "https://api.ipify.org/?format=json";
const detailInfoURL = "http://ip-api.com/";
const arr = [];
const info = document.createElement("div");
const buttonIp = document.querySelector('.btn-danger');

buttonIp.addEventListener("click", (event) => {
    async function request() {
        try {
            let response = await fetch(requestURL)
            let {ip} = await response.json();
            console.log(ip);
            return ip
            
        } catch(err) {
            console.warn(err);
        }
    
        
    }
    
    info.innerText = "";

    async function details() {
    
        try {
    
            const localIp = await request();
            let test = await fetch(`http://ip-api.com/json/${localIp}`);
            let test2 = await test.json();
            const {timezone, country, region, city, regionName} = test2; //району немає
            
    
            info.setAttribute("class", "information");
            document.body.append(info);

    
            info.insertAdjacentHTML("beforeend", `   <p class="geographyInfo">Timezone: ${timezone}</p>
            <p class="geographyInfo">Country: ${country}</p>
            <p class="geographyInfo">Region: ${region}</p>
            <p class="geographyInfo">City: ${city}</p>
            <p class="geographyInfo">Region Name: ${regionName}</p>`);
        } catch(err) {
            console.warn(err);
        }  
    }
    
    details();
});



// https://stackoverflow.com/questions/74729131/get-info-from-a-another-function-with-an-async-method


// const userInstance = axios.create({ 
//     baseURL: 'https://ajax.test-danit.com/api/json/users',
//     headers: {
//         'API-CODE': '1234'
//     }
//  });


//  postsInstance.get('/3').then(({ data }) => console.log(data));
//  userInstance.get('/2').then(({ data }) => console.log(data));